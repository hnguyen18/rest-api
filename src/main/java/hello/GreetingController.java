package hello;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private static Map<Long, Greeting> db = new HashMap<Long, Greeting>();

    @GetMapping("/greeting")
    public Greeting createGreeting(@RequestParam(value="name", defaultValue="World") String name) {
        Greeting g = new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
        db.put(g.getId(), g);
        return g;
    }
    
    @GetMapping("/greeting/{id}")
    public Greeting getGreeting(@PathVariable long id) {
        Greeting g = db.get(id);
        return g;
    }
    
    @PutMapping("/greeting/{id}/{name}")
    public void putGreeting(@PathVariable long id, @PathVariable String name) {
        db.put(id, new Greeting(id, name));
    }    
    
}